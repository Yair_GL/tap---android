package com.example.sonidos;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    private ImageButton b1,b2,b3,b4,b5,b6;
    private MediaPlayer m;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = findViewById(R.id.cerdo);
        b2 = findViewById(R.id.chimpance);
        b3 = findViewById(R.id.leon);
        b4 = findViewById(R.id.zorro);
        b5 = findViewById(R.id.vaca);
        b6 = findViewById(R.id.foca);


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stop();
                sonidoCerdo();
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stop();
                sonidoChimpance();
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stop();
                sonidoLeon();
            }
        });

        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stop();
                sonidoZorro();
            }
        });

        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stop();
                sonidoVaca();
            }
        });

        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stop();
                sonidoFoca();
            }
        });

    }

    public void sonidoCerdo(){
            m = MediaPlayer.create(this, R.raw.scerdo);
            m.start();
        }


    public void sonidoChimpance(){
            m = MediaPlayer.create(this, R.raw.schimpance);
            m.start();
    }

    public void sonidoLeon(){
            m = MediaPlayer.create(this, R.raw.sleon);
            m.start();
    }

    public void sonidoZorro(){
            m = MediaPlayer.create(this, R.raw.szorro);
            m.start();
    }

    public void sonidoVaca(){
            m = MediaPlayer.create(this, R.raw.svaca);
            m.start();
    }

    public void sonidoFoca(){
        m = MediaPlayer.create(this, R.raw.sfoca);
        m.start();
    }

    @Override
    protected void onStop() {
        stop();
        super.onStop();
    }

    public void stop(){
        if(m!=null){
            m.stop();
            m.release();
            m=null;
        }
    }

}
